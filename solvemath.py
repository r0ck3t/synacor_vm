#!/usr/bin/env python3
import itertools

def testmath(iteration):
    return iteration[0] + iteration[1] * iteration[2]**2 + iteration[3]**3 - iteration[4] == 399

validNumbers = [2, 3, 5, 7, 9]

for iteration in itertools.permutations(validNumbers):
    if testmath(iteration) == True:
        print iteration
        break
         