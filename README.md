# synacor_vm

Rust version of https://challenge.synacor.com

#Improvements to make
- Debugging mode is not automated/scriptable this should be changed. Technically this could be fixed and the reason that any of the automation works is due to how the VM processes input. This could be done external to the program which is probably much cleaner.
- Code is hideous and not very Rust-like. Modules of the VM need to be broken out to modules.
# Vault
Make 30 via the shortest path

Column 1 | Column 2 | Column 3 | Column 4
:---: | :---: | :---: | :---:
| * | 8 |  - | 1
4 | * | 11 | *
+ | 4 |  - | 18
22 | - |  9 | *