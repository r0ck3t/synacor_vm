#!/usr/bin/env python3

import struct

TEST_BINARY = "9,32768,32769,4,19,32768"

with open('test.bin', 'wb') as f:
	code = map(int, TEST_BINARY.split(','))
	for c in code:
		f.write(struct.pack('<H', c))
