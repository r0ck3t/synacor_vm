#!/usr/bin/env python3
import pexpect
import sys
import time

MAX_INT = 32768
TESTED = 28000

# This is very slow overall. This method could be parallelized very easily by splitting up each attempt into it's own thread.
# Nothing here shares state. I have lots of time to wait for this though, so it was simple to throw together.cccc

for x in range(TESTED, MAX_INT):
    print("Running {}".format(x), file=sys.stderr)
    print("Running {}".format(x))

    brute = pexpect.spawn('cargo run challenge.bin', encoding='utf-8')
    # brute.logfile = sys.stdout

    brute.expect("Register 7 :")

    brute.sendline("change 7 {}".format(x))

    brute.expect("Register 7 :")

    brute.sendline("exit")

    brute.expect("------")

    brute.sendline("use teleporter")

    try:
        brute.expect("1 billion years.", timeout=2)
        brute.kill(0)
    except:
        print(brute.before)
        brute.kill(0)


