extern crate synacor_vm;

use std::env;
use std::process;

fn main() {
    println!("Welcome to the Synacor VM!\n--------------------------------------------------");
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        eprintln!("Invalid number of arguments.\nPlease enter a single file for the VM to parse.");

        process::exit(1);
    }

    let automation_string = String::from("take tablet\ndoorway\nnorth\nnorth\nbridge\ncontinue\ndown\neast\ntake empty lantern\nwest\nwest\npassage\nladder\nwest\nsouth\nnorth\ntake can\nuse can\nwest\nladder\ndarkness\nuse lantern\ncontinue\nwest\nwest\nwest\nwest\nnorth\ntake red coin\nnorth\neast\ntake concave coin\ndown\ntake corroded coin\nup\nwest\nwest\ntake blue coin\nup\ntake shiny coin\ndown\neast\nuse blue coin\nuse red coin\nuse shiny coin\nuse concave coin\nuse corroded coin\nnorth\ntake teleporter\nuse teleporter\ntake business card\ntake strange book\nDEBUG\nbreakpoint 5489\nchange 7 25734\nexit\nuse teleporter\nchange 0 6\nmove_exec 5491\n");

    if let Err(e) = synacor_vm::run(&args, Some(automation_string)) {
        println!("VM Error : {}", e);

        process::exit(1);
    }

    println!("\n--------------------------------------------------\nSynacor VM exited successfully!");
}
