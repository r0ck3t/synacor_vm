extern crate byteorder;

use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};

use std::error::Error;
use std::fs::File;
use std::io::{self, ErrorKind, BufRead};
use std::process;


const MAX_INT : u32 = 32768;
const INVALID_INT : u32 = 32776;

fn check_user_input(user_input: &str, check_against: &str) -> bool {
    if user_input.len() < check_against.len() {
        return false
    }

    if &user_input[..check_against.len()] == check_against {
        return true;
    }

    false
}

#[derive(Debug, Eq, PartialEq)]
struct VM {
    registers: [u16; 8],
    memory: Vec<u16>,
    stack: Vec<u16>,
    instr_ptr : usize,
    processing_string : String,
    debug: bool,
    breakpoints: Vec<u16>,
}

impl VM {
    pub fn new() -> VM {
        VM {
            registers: [0; 8],
            memory: vec![],
            stack: vec![],
            instr_ptr: 0,
            processing_string: String::new(),
            debug: false,
            breakpoints: vec![],
        }
    }

    pub fn parse_file(&mut self, args: &[String]) -> Result<&VM, Box<Error>> {
        let filename = &args[1];
        let mut f = File::open(filename)?;

        loop {
            match f.read_u16::<LittleEndian>() {
                Ok(word) => self.memory.push(word),
                Err(ref error) if error.kind() == ErrorKind::UnexpectedEof => break,
                Err(error) => {
                    return Err(Box::new(error))
                }
            };
        }

        Ok(self)
    }

    fn change_register_value(&mut self, register: u16, value: u16) -> Result<(), Box<Error>> {
        if register >= 8 {
            panic!("register index out of bounds");
        }

        if value as u32 > MAX_INT {
            panic!("over max value provided");
        }

        println!("Changing register {} to {}", register, value);

        self.registers[register as usize] = value;


        Ok(())
    }

    fn change_instr_pointer(&mut self, value: u16) -> Result<(), Box<Error>> {
        if value as u32 > MAX_INT {
            panic!("over max value provided to change_instr_pointer");
        }

        println!("Changing instruction pointer to {}", value);

        self.instr_ptr = value as usize;

        Ok(())
    }

    fn dump_memory(&self) -> Result<(), Box<Error>> {
        let mut f = File::create("memory_dump.bin")?;

        for word in &self.memory {
            f.write_u16::<LittleEndian>(*word)?;
        }

        Ok(())
    }

    fn breakpoint(&mut self, value: u16) -> Result<(), Box<Error>> {
        if value as u32 > self.memory.len() as u32 {
            panic!("breakpoint set past the end of program memory");
        }

        println!("Setting breakpoint at : {}", value);

        self.breakpoints.push(value);

        Ok(())
    }

    fn debug_interface(&mut self) -> Result<(), Box<Error>> {
        println!("----------------------------------\nWelcome to the Synacor VM Introspection Interface!");

        // This is just scripting cleanup
        // for x in "EBUG".chars() {
        //     self.processing_string.remove(0);
        // }

        loop {
            println!("\nCurrent VM Register state:");
            for (i, c) in self.registers.iter().enumerate() {
                println!("Register {} : {}", i, c);
            }
            println!("");

            let mut debug_response = String::new();
            let stdin = io::stdin();
            stdin.lock().read_line(&mut debug_response).unwrap();
            // if self.processing_string.len() == 0 {

            // } else {
                
            //     let mut debug_response = self.processing_string.split("\n").collect::<Vec<&str>>();
            //     println!("{}", debug_response[0]);
            // }


            if check_user_input(&debug_response[..], "change") {
                let mut split : Vec<&str> = debug_response.split(" ").collect();
                if split.len() == 3 {
                    // Ugly error handling, this needs to be fixed
                    let reg : u16 = split[1].trim().parse().unwrap();
                    let val : u16 = split[2].trim().parse().unwrap();

                    self.change_register_value(reg, val);
                }
            }
            else if check_user_input(&debug_response[..], "move_exec") {
                let mut split : Vec<&str> = debug_response.split(" ").collect();
                if split.len() == 2 {
                    let val : u16 = split[1].trim().parse().unwrap();
                    self.change_instr_pointer(val);
                }
            }
            else if check_user_input(&debug_response[..], "dumpmem") {
                self.dump_memory();
            }
            else if check_user_input(&debug_response[..], "breakpoint") {
                let mut split : Vec<&str> = debug_response.split(" ").collect();
                if split.len() == 2 {
                    let val : u16 = split[1].trim().parse().unwrap();
                    self.breakpoint(val);
                }
            }
            else if check_user_input(&debug_response[..], "exit") {
                break;
            }

        }


        println!("Leaving the Synacor VM Introspection Interface!\n----------------------------------");
        Ok(())
    }

    pub fn parse_instr(&mut self, debug: u8) {
        loop {
            // println!("Instruction Pointer : {}", self.instr_ptr);
            // println!("Instruction : {}", self.memory[self.instr_ptr]);
            if self.instr_ptr >= self.memory.len() {
                break;
            }

            for bp in &self.breakpoints {
                if *bp as usize == self.instr_ptr {
                    self.debug = true;
                }
            }

            if self.debug {
                self.debug_interface();
                self.debug = false;
            }

            // if self.instr_ptr > 5000 && self.instr_ptr < 6000 {
            //     println!("Executing Instruction @ {}", self.instr_ptr);

            // }

            match self.memory[self.instr_ptr] {
                0 => {

                    if debug == 1 {
                        println!("HALT");
                    }

                    self.op_halt();
                },
                1 => {
                    let arg1 = self.memory[self.instr_ptr + 1];
                    let arg2 = self.memory[self.instr_ptr + 2];

                    if debug == 1 {
                        println!("SET : {}, {}", arg1, arg2);
                    }

                    self.op_set(arg1, arg2);
                },
                2 => {
                    let arg1 = self.memory[self.instr_ptr + 1];

                    if debug == 1 {
                        println!("PUSH : {}", arg1);
                    }

                    self.op_push(arg1);
                },
                3 => {
                    let arg1 = self.memory[self.instr_ptr + 1];

                    if debug == 1 {
                        println!("POP : {}", arg1);
                    }

                    self.op_pop(arg1);
                }
                4 => {
                    let arg1 = self.memory[self.instr_ptr + 1];
                    let arg2 = self.memory[self.instr_ptr + 2];
                    let arg3 = self.memory[self.instr_ptr + 3];

                    if debug == 1 {
                        println!("EQ : {}, {}, {}", arg1, arg2, arg3);
                    }

                    self.op_eq(arg1, arg2, arg3);
                },
                5 => {
                    let arg1 = self.memory[self.instr_ptr + 1];
                    let arg2 = self.memory[self.instr_ptr + 2];
                    let arg3 = self.memory[self.instr_ptr + 3];

                    if debug == 1 {
                        println!("GT : {}, {}, {}", arg1, arg2, arg3);
                    }

                    self.op_gt(arg1, arg2, arg3);
                },
                6 => {
                    let arg1 = self.memory[self.instr_ptr + 1];

                    if debug == 1 {
                        println!("JMP : {}", arg1);
                    }

                    self.op_jmp(arg1);
                },
                7 => {
                    let arg1 = self.memory[self.instr_ptr + 1];
                    let arg2 = self.memory[self.instr_ptr + 2];

                    if debug == 1 {
                        println!("JT : {}, {}", arg1, arg2);
                    }

                    self.op_jt(arg1, arg2);
                },
                8 => {
                    let arg1 = self.memory[self.instr_ptr + 1];
                    let arg2 = self.memory[self.instr_ptr + 2];

                    if debug == 1 {
                        println!("JF : {}, {}", arg1, arg2);
                    }

                    self.op_jf(arg1, arg2);
                },
                9 => {
                    let arg1 = self.memory[self.instr_ptr + 1];
                    let arg2 = self.memory[self.instr_ptr + 2];
                    let arg3 = self.memory[self.instr_ptr + 3];

                    if debug == 1 {
                        println!("ADD : {}, {}, {}", arg1, arg2, arg3);
                    }

                    self.op_add(arg1, arg2, arg3);
                },
                10 => {
                    let arg1 = self.memory[self.instr_ptr + 1];
                    let arg2 = self.memory[self.instr_ptr + 2];
                    let arg3 = self.memory[self.instr_ptr + 3];

                    if debug == 1 {
                        println!("MULT : {}, {}, {}", arg1, arg2, arg3);
                    }

                    self.op_mult(arg1, arg2, arg3);
                },
                11 => {
                    let arg1 = self.memory[self.instr_ptr + 1];
                    let arg2 = self.memory[self.instr_ptr + 2];
                    let arg3 = self.memory[self.instr_ptr + 3];

                    if debug == 1 {
                        println!("MOD : {}, {}, {}", arg1, arg2, arg3);
                    }

                    self.op_mod(arg1, arg2, arg3);
                },
                12 => {
                    let arg1 = self.memory[self.instr_ptr + 1];
                    let arg2 = self.memory[self.instr_ptr + 2];
                    let arg3 = self.memory[self.instr_ptr + 3];

                    if debug == 1 {
                        println!("AND : {}, {}, {}", arg1, arg2, arg3);
                    }

                    self.op_and(arg1, arg2, arg3);
                },
                13 => {
                    let arg1 = self.memory[self.instr_ptr + 1];
                    let arg2 = self.memory[self.instr_ptr + 2];
                    let arg3 = self.memory[self.instr_ptr + 3];

                    if debug == 1 {
                        println!("OR : {}, {}, {}", arg1, arg2, arg3);
                    }

                    self.op_or(arg1, arg2, arg3);
                },
                14 => {
                    let arg1 = self.memory[self.instr_ptr + 1];
                    let arg2 = self.memory[self.instr_ptr + 2];

                    if debug == 1 {
                        println!("NOT : {}, {}", arg1, arg2);
                    }

                    self.op_not(arg1, arg2);
                },
                15 => {
                    let arg1 = self.memory[self.instr_ptr + 1];
                    let arg2 = self.memory[self.instr_ptr + 2];

                    if debug == 1 {
                        println!("RMEM : {}, {}", arg1, arg2);
                    }

                    self.op_rmem(arg1, arg2);
                },
                16 => {
                    let arg1 = self.memory[self.instr_ptr + 1];
                    let arg2 = self.memory[self.instr_ptr + 2];

                    if debug == 1 {
                        println!("WMEM : {}, {}", arg1, arg2);
                    }

                    self.op_wmem(arg1, arg2);
                },
                17 => {
                    let arg1 = self.memory[self.instr_ptr + 1];

                    if debug == 1 {
                        println!("CALL : {}", arg1);
                    }

                    self.op_call(arg1);
                },
                18 => {

                    if debug == 1 {
                        println!("RET");
                    }

                    self.op_ret();
                },
                19 => {
                    let arg1 = self.memory[self.instr_ptr + 1];

                    if debug == 1 {
                        println!("OUT : {}", arg1);
                    }

                    self.op_out(arg1);
                },
                20 => {
                    let arg1 = self.memory[self.instr_ptr + 1];

                    if debug == 1 {
                        println!("IN : {}", arg1);
                    }

                    self.op_in(arg1);
                },
                21 => {

                    if debug == 1 {
                        println!("NOOP");
                    }

                    self.op_noop();
                },
                _ => {
                    panic!("bad instruction : {}", self.memory[self.instr_ptr]);
                }
            };
        }
    }

    fn helper_get_reg(&self, register: u16) -> u16 {
        let register = register as u32;

        if register >= INVALID_INT {
            panic!("invalid number found : {}", register)
        }

        (register % MAX_INT) as u16
    }

    fn helper_get_val(&self, register: u16) -> u16 {
        let register = register as u32;

        if register >= INVALID_INT {
            panic!("invalid number found : {}", register)
        }
        
        if register >= MAX_INT {
            let register = register % MAX_INT;

            return self.registers[register as usize]
        }
        register as u16
    }

    // stop execution and terminate the program
    fn op_halt(&self) -> Result<(), Box<Error>>{
        println!("PROCESSOR HALT");
        process::exit(1)
    }

    // set register <a> to the value of <b>
    fn op_set(&mut self, reg_a: u16, reg_b: u16) -> Result<(), Box<Error>>{
        // We know the first arg is a register, so we need to find out which register
        let reg_a = self.helper_get_reg(reg_a);

        let reg_b = self.helper_get_val(reg_b);

        self.registers[reg_a as usize] = reg_b;

        self.instr_ptr = self.instr_ptr + 3;

        Ok(())
    }

    // push <a> onto the stack
    fn op_push(&mut self, reg_a: u16) -> Result<(), Box<Error>>{
        let reg_a = self.helper_get_val(reg_a);

        self.stack.push(reg_a);

        self.instr_ptr = self.instr_ptr + 2;

        Ok(())
    }

    // remove the top element from the stack and write it into <a>; empty stack = error
    fn op_pop(&mut self, reg_a: u16) -> Result<(), Box<Error>>{
        let reg_a = self.helper_get_reg(reg_a);

        self.registers[reg_a as usize] = match self.stack.pop().ok_or("attempting to pop from empty stack") {
            Ok(value) => value,
            // We are panicing here because we don't have a stack anymore and we can't pop off of the empty Vec
            Err(e) => panic!(e),
        };

        self.instr_ptr = self.instr_ptr + 2;

        Ok(())
    }

    // set <a> to 1 if <b> is equal to <c>; set it to 0 otherwise
    fn op_eq(&mut self, reg_a: u16, reg_b: u16, reg_c: u16) -> Result<(), Box<Error>>{
        // We are assuming that a here is a register
        let reg_a = self.helper_get_reg(reg_a);

        let reg_b = self.helper_get_val(reg_b);
        let reg_c = self.helper_get_val(reg_c);

        if reg_b == reg_c {
            self.registers[reg_a as usize] = 1 as u16;
        } else {
            self.registers[reg_a as usize] = 0 as u16;
        }

        self.instr_ptr = self.instr_ptr + 4;

        Ok(())
    }

    // set <a> to 1 if <b> is greater than <c>; set it to 0 otherwise
    fn op_gt(&mut self, reg_a: u16, reg_b: u16, reg_c: u16) -> Result<(), Box<Error>>{
        // We are assuming that a here is a register
        let reg_a = self.helper_get_reg(reg_a);

        let reg_b = self.helper_get_val(reg_b);
        let reg_c = self.helper_get_val(reg_c);

        if reg_b > reg_c {
            self.registers[reg_a as usize] = 1 as u16;
        } else {
            self.registers[reg_a as usize] = 0 as u16;
        }

        self.instr_ptr = self.instr_ptr + 4;

        Ok(())
    }

    // jump to <a>
    fn op_jmp(&mut self, jump_loc: u16) -> Result<(), Box<Error>>{
        // We don't need to do much here, just change the instruction pointer and move along
        self.instr_ptr = jump_loc as usize;

        Ok(())
    }

    // if <a> is nonzero, jump to <b>
    fn op_jt(&mut self, reg_a: u16, jump_loc: u16) -> Result<(), Box<Error>>{
        let reg_a = self.helper_get_val(reg_a);

        if reg_a != 0 {
            self.instr_ptr = jump_loc as usize;
        } else {
            self.instr_ptr = self.instr_ptr + 3;
        }

        Ok(())
    }

    // if <a> is zero, jump to <b>
    fn op_jf(&mut self, reg_a: u16, jump_loc: u16) -> Result<(), Box<Error>>{
        let reg_a = self.helper_get_val(reg_a);

        if reg_a == 0 {
            self.instr_ptr = jump_loc as usize;
        } else {
            self.instr_ptr = self.instr_ptr + 3;
        }

        Ok(())
    }

    // assign into <a> the sum of <b> and <c> (modulo 32768)
    fn op_add(&mut self, reg_a: u16, reg_b: u16, reg_c: u16) -> Result<(), Box<Error>>{
        let reg_a = self.helper_get_reg(reg_a);
        let reg_b = self.helper_get_val(reg_b);
        let reg_c = self.helper_get_val(reg_c);

        self.registers[reg_a as usize] = (((reg_b + reg_c) as u32) % MAX_INT) as u16;

        self.instr_ptr = self.instr_ptr + 4;

        Ok(())
    }

    // store into <a> the product of <b> and <c> (modulo 32768)
    fn op_mult(&mut self, reg_a: u16, reg_b: u16, reg_c: u16) -> Result<(), Box<Error>>{
        let reg_a = self.helper_get_reg(reg_a);
        let reg_b = self.helper_get_val(reg_b);
        let reg_c = self.helper_get_val(reg_c);

        self.registers[reg_a as usize] = (((reg_b as u32) * (reg_c as u32)) % MAX_INT) as u16;

        self.instr_ptr = self.instr_ptr + 4;

        Ok(())
    }

    // store into <a> the remainder of <b> divided by <c>
    fn op_mod(&mut self, reg_a: u16, reg_b: u16, reg_c: u16) -> Result<(), Box<Error>>{
        let reg_a = self.helper_get_reg(reg_a);
        let reg_b = self.helper_get_val(reg_b);
        let reg_c = self.helper_get_val(reg_c);

        self.registers[reg_a as usize] = reg_b % reg_c;

        self.instr_ptr = self.instr_ptr + 4;

        Ok(())
    }

    // stores into <a> the bitwise and of <b> and <c>
    fn op_and(&mut self, reg_a: u16, reg_b: u16, reg_c: u16) -> Result<(), Box<Error>>{
        let reg_a = self.helper_get_reg(reg_a);
        let reg_b = self.helper_get_val(reg_b);
        let reg_c = self.helper_get_val(reg_c);

        self.registers[reg_a as usize] = reg_b & reg_c;

        self.instr_ptr = self.instr_ptr + 4;

        Ok(())
    }

    // stores into <a> the bitwise or of <b> and <c>
    fn op_or(&mut self, reg_a: u16, reg_b: u16, reg_c: u16) -> Result<(), Box<Error>>{
        let reg_a = self.helper_get_reg(reg_a);
        let reg_b = self.helper_get_val(reg_b);
        let reg_c = self.helper_get_val(reg_c);

        self.registers[reg_a as usize] = reg_b | reg_c;

        self.instr_ptr = self.instr_ptr + 4;

        Ok(())
    }

    // stores 15-bit bitwise inverse of <b> in <a>
    fn op_not(&mut self, reg_a: u16, reg_b: u16) -> Result<(), Box<Error>>{
        let reg_a = self.helper_get_reg(reg_a);
        let reg_b = self.helper_get_val(reg_b);

        self.registers[reg_a as usize] = (!reg_b) & 0b111111111111111;

        self.instr_ptr = self.instr_ptr + 3;

        Ok(())
    }

    // read memory at address <b> and write it to <a>
    fn op_rmem(&mut self, reg_a: u16, reg_b: u16) -> Result<(), Box<Error>>{
        let reg_a = self.helper_get_reg(reg_a);
        let reg_b = self.helper_get_val(reg_b);

        // THIS IS AN UNVERIFIED READ/WRITE CHECK THIS
        self.registers[reg_a as usize] = self.memory[reg_b as usize];

        self.instr_ptr = self.instr_ptr + 3;

        Ok(())
    }

    // write the value from <b> into memory at address <a>
    fn op_wmem(&mut self, reg_a: u16, reg_b: u16) -> Result<(), Box<Error>>{
        let reg_a = self.helper_get_val(reg_a);
        let reg_b = self.helper_get_val(reg_b);

        self.memory[reg_a as usize] = reg_b;

        self.instr_ptr = self.instr_ptr + 3;

        Ok(())
    }

    // write the address of the next instruction to the stack and jump to <a>
    fn op_call(&mut self, jump_loc: u16) -> Result<(), Box<Error>>{
        let jump_loc = self.helper_get_val(jump_loc);

        self.stack.push((self.instr_ptr + 2) as u16);

        self.instr_ptr = jump_loc as usize;

        Ok(())
    }

    //remove the top element from the stack and jump to it; empty stack = halt
    fn op_ret(&mut self) -> Result<(), Box<Error>>{
        self.instr_ptr = match self.stack.pop().ok_or("attempting to pop from empty stack") {
            Ok(value) => value as usize,
            // We are panicing here because we don't have a stack anymore and we can't pop off of the empty Vec
            Err(e) => panic!(e),
        };

        Ok(())
    }

    //write the character represented by ascii code <a> to the terminal
    fn op_out(&mut self, reg_a: u16) -> Result<(), Box<Error>>{
        let reg_a = self.helper_get_val(reg_a);

        print!("{}", reg_a as u8 as char);

        self.instr_ptr = self.instr_ptr + 2;

        Ok(())
    }

    //read a character from the terminal and write its ascii code to <a>; it can be assumed that once input starts, it will continue until a newline is encountered; this means that you can safely read whole lines from the keyboard and trust that they will be fully read
    fn op_in(&mut self, reg_a: u16) -> Result<(), Box<Error>>{
        let reg_a = self.helper_get_reg(reg_a);

        if self.processing_string.len() == 0 {
            let stdin = io::stdin();
            stdin.lock().read_line(&mut self.processing_string).unwrap();

            self.processing_string = String::from(self.processing_string.trim());
            self.processing_string.push_str("\n");

            if self.processing_string.len() >= "DEBUG\n".len() {
                if &self.processing_string[.."DEBUG\n".len()] == "DEBUG\n" {
                    println!("Debug Mode Activated!");
                    self.debug = true;
                    // self.processing_string.clear();
                    // self.processing_string.push_str("\n");
                }
            }

            self.registers[reg_a as usize] = self.processing_string.remove(0) as u16;
        } else {

            if self.processing_string.len() >= "DEBUG\n".len() {
                if &self.processing_string[.."DEBUG\n".len()] == "DEBUG\n" {
                    println!("Debug Mode Activated!");
                    self.debug = true;
                    // self.processing_string.clear();
                    // self.processing_string.push_str("\n");
                }
            }

            self.registers[reg_a as usize] = self.processing_string.remove(0) as u16;
        }

        self.instr_ptr = self.instr_ptr + 2;

        Ok(())
    }

    //no operation
    fn op_noop(&mut self) -> Result<(), Box<Error>>{
        self.instr_ptr = self.instr_ptr + 1;

        Ok(())
    }
}


pub fn run(args: &[String], starting_str: Option<String>) -> Result<(), Box<Error>> {
    let mut vm = VM::new();
    vm.parse_file(args)?;

    match starting_str {
        Some(string) => vm.processing_string = string,
        None => vm.processing_string = String::new(),
    }

    vm.parse_instr(0);

    Ok(())
}

